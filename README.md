# Fragment Navigation Drawer

> ## Note
>
> This code is **taken/copyed/implemented** from [codepath](https://guides.codepath.com/android/fragment-navigation-drawer#add-navigation-header) website. Just for learning perposes....


### Algorithm 
  * **Class List :**
    * MainActivity
    * FirstFragment
    * SecondFragment
    * ThirdFragment
    <br>
* **Layout List: **
  * activity_first_fragment
  * activity_main
  * activity_second_fragment
  * activity_third_fragment
  * nav_header
  * toolbar

### Usage 

 * Matetial Design `implementation 'com.google.android.material:material:1.4.0'` 


### Changelogs

  * Git Init 
  * Nothing has changes **just implemented method from page**
  * Fragment Bug has been fixed ! 

### Screenshot 
![luca](app_shot.png)

### TESTED AND OK
### WONDERFUL IMPLEMENTATION AND ADVANCED 

**Source** <br>

[Code Path](https://guides.codepath.com/android/fragment-navigation-drawer#add-navigation-header)
